function check() {
    /**
     * This function will check given number is odd or even.
     */
    var num1 = document.getElementById("num1");
    var number1 = parseFloat(num1.value);
    if (number1 % 2 == 0) {
        alert("The entered number is even");
    }
    else {
        alert("The entered number is odd");
    }
}
//# sourceMappingURL=CheckOddEven.js.map