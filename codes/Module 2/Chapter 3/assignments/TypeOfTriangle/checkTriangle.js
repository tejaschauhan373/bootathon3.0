function check() {
    /**
     * This function will  find the type of triagle .
     */
    var s1 = document.getElementById("s1");
    var s2 = document.getElementById("s2");
    var s3 = document.getElementById("s3");
    var a = parseFloat(s1.value);
    var b = parseFloat(s2.value);
    var c = parseFloat(s3.value);
    if (a == b && b == c) {
        document.getElementById("display").innerHTML = "The Triangle is Equilateral!";
    }
    else if ((a == b && a != c) || (b == c && b != a) || (a == c && c != b)) {
        if (Math.pow(a, 2) == (Math.pow(b, 2) + Math.pow(c, 2)) || Math.pow(b, 2) == (Math.pow(a, 2) + Math.pow(c, 2)) || Math.pow(c, 2) == (Math.pow(a, 2) + Math.pow(b, 2))) {
            document.getElementById("display").innerHTML = "The Triangle is Isosceles Right Angled Triangle!";
        }
        else {
            document.getElementById("display").innerHTML = "The Triangle is Isosceles Triangle!";
        }
    }
    else if (a != b && b != c && a != c) {
        if (Math.pow(a, 2) == (Math.pow(b, 2) + Math.pow(c, 2)) || Math.pow(b, 2) == (Math.pow(a, 2) + Math.pow(c, 2)) || Math.pow(c, 2) == (Math.pow(a, 2) + Math.pow(b, 2))) {
            document.getElementById("display").innerHTML = "The Triangle is Scalene Right Angled Triangle!";
        }
        else {
            document.getElementById("display").innerHTML = "The Triangle is Scalene!";
        }
    }
    else {
        document.getElementById("display").innerHTML = "Input Error";
    }
}
//# sourceMappingURL=checkTriangle.js.map