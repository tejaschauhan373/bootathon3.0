function dynamic() {
    var input = document.getElementById("input");
    var table = document.getElementById("table_1");
    var num = +input.value; // covert string to float data type
    var count = 1;
    // delete the all row from past table if there are.
    while (table.rows.length > 1) {
        table.deleteRow(1);
    }
    // creating new row for new input
    for (count = 1; count <= num; count++) {
        //create row in table
        var row = table.insertRow();
        // create first cell element 
        //here  we have also enabled readonly attribute so user can not change values of input field.  
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "number";
        text.style.backgroundColor = "yellow;";
        text.id = "t" + count;
        text.style.textAlign = "center";
        text.value = count.toString();
        cell.appendChild(text);
        // create second cell element 
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "number";
        text.id = "tt" + count;
        text.style.backgroundColor = "yellow;";
        text.style.textAlign = "center";
        cell.appendChild(text);
    }
}
function square() {
    var table = document.getElementById("table_1");
    var count = 1;
    while (count < table.rows.length) {
        var row_num = document.getElementById("t" + count);
        var num = +row_num.value;
        var squre_num = document.getElementById("tt" + count);
        squre_num.value = (num * num).toString();
        count++;
    }
}
//# sourceMappingURL=dynamic.js.map