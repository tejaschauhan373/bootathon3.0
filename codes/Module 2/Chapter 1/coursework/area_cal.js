function area() {
    let t11 = document.getElementById("t11");
    let t12 = document.getElementById("t12");
    let t21 = document.getElementById("t21");
    let t22 = document.getElementById("t22");
    let t31 = document.getElementById("t31");
    let t32 = document.getElementById("t32");
    let ans = document.getElementById("ans");
    var x1 = parseFloat(t11.value);
    var y1 = parseFloat(t12.value);
    var x2 = parseFloat(t21.value);
    var y2 = parseFloat(t22.value);
    var x3 = parseFloat(t31.value);
    var y3 = parseFloat(t32.value);
    console.log("x1=" + x1);
    console.log("y1=" + y1);
    console.log("x2=" + x2);
    console.log("y2=" + y2);
    console.log("x3=" + x3);
    console.log("y3=" + y3);
    if (isNaN(x1) && isNaN(y1) || isNaN(x2) || isNaN(y2) || isNaN(x3) || isNaN(y3)) {
        alert("Please enter numeric values only.");
    }
    else {
        var a = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
        console.log(a);
        var b = Math.sqrt(Math.pow((x1 - x3), 2) + Math.pow((y1 - y3), 2));
        console.log(b);
        var c = Math.sqrt(Math.pow((x3 - x2), 2) + Math.pow((y3 - y2), 2));
        console.log(c);
        var s = (a + b + c) / 2;
        var area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
        ans.innerHTML = "The area is :" + area.toString();
    }
}
//# sourceMappingURL=area_cal.js.map