function areaOfTriangle() {
    /**
     *   This function computes the area of triangle.
     *   Also check all values entered by user are numeric or not.
     *   If atleast one of these is not numeric it will generate notification to enter numeric  value.
     *   else compute the area of triangle and return it.
    */
    let t11 = document.getElementById("t11");
    let t12 = document.getElementById("t12");
    let t21 = document.getElementById("t21");
    let t22 = document.getElementById("t22");
    let t31 = document.getElementById("t31");
    let t32 = document.getElementById("t32");
    let ans = document.getElementById("ans");
    // convert values into float.
    var x1 = parseFloat(t11.value);
    var y1 = parseFloat(t12.value);
    var x2 = parseFloat(t21.value);
    var y2 = parseFloat(t22.value);
    var x3 = parseFloat(t31.value);
    var y3 = parseFloat(t32.value);
    console.log("x1=" + x1);
    console.log("y1=" + y1);
    console.log("x2=" + x2);
    console.log("y2=" + y2);
    console.log("x3=" + x3);
    console.log("y3=" + y3);
    if (isNaN(x1) || isNaN(y1) || isNaN(x2) || isNaN(y2) || isNaN(x3) || isNaN(y3)) {
        alert("Please enter numeric values only."); // Give notification , if anyone of these values is not numeric.
    }
    else {
        //  compute the area of Triangle.
        var a = Math.sqrt(Math.abs(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2)));
        console.log(a);
        var b = Math.sqrt(Math.abs(Math.pow((x1 - x3), 2) + Math.pow((y1 - y3), 2)));
        console.log(b);
        var c = Math.sqrt(Math.abs(Math.pow((x3 - x2), 2) + Math.pow((y3 - y2), 2)));
        console.log(c);
        var s = (a + b + c) / 2;
        var area = Math.sqrt(s * (s - a) * (s - b) * (s - c)); //  Apply MATH.sqrt to find the square root of  value.
        ans.innerHTML = "Area of Triangle :" + area.toString(); // convert value of variable area into string and display it.
    }
}
//# sourceMappingURL=triangle.js.map