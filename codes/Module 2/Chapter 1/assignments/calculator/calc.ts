function add(value)
{
    /**
     *  This function add the value when user press any key on the calculator.
     * 
     */
    let ans : HTMLInputElement=<HTMLInputElement>document.getElementById("ans");
    ans.value+=value;
    console.log(ans.value);
}
function calculate()
{
    /**
     *      This function calculate the answer of string given by user.
     *      It will use functions of Math module. 
     *    
     */
    try
    {
        let ans : HTMLInputElement=<HTMLInputElement>document.getElementById("ans");
        console.log(ans.value);
        
        // we will store string in regular expression to replace it and pass that object to all that.
        var re = /tan\(/gi;     //replace all "tan" by "Math.tan" in string
        var newstr = ans.value.replace(re, "Math.tan(");
        re = /sin\(/gi;         //replace all "sin" by "Math.sin" in string
        newstr = newstr.replace(re, "Math.sin(");
        re = /cos\(/gi;         //replace all "cos" by "Math.cos" in string
        newstr = newstr.replace(re, "Math.cos(");
        re = /\^/gi;            //replace all "^" by "**" in string
        newstr = newstr.replace(re, "**")
        console.log(newstr);
        re = /degree\=/gi;       //replace all "degree=" by "Math.PI/180" in string
        newstr = newstr.replace(re, "Math.PI/180*")  // convert degree to radian because function takes radian value.
        re = /sqrt\(/gi;      //replace all "sqrt(" by "Math.sqrt(" in string
        newstr = newstr.replace(re, "Math.sqrt(")
        re = /radian\=/gi;         //replace all "radian=" by "" in string
        newstr = newstr.replace(re, "") // because it takes radian values already.

        console.log(newstr);

        ans.value= eval(newstr); //evaluate the string and store that value.
    }
    catch(Error)
    {   
        // When user enter invalid input it will generate the error and we will notify to enter correct input.
        alert("enter valid values");
        // all kind of error will be handled by this block.
    }
}
function backspace()
{   
    /**
    *  This function remove last elment from the screen of calculator or input string.    
    */
    let ans : HTMLInputElement=<HTMLInputElement>document.getElementById("ans");
    ans.value=ans.value.substring(0, ans.value.length - 1);   
    console.log(ans.value);
}
function clearall()
{
    /**
     *    This function clear the screen and  assign empty string. 
     */
    let ans : HTMLInputElement=<HTMLInputElement>document.getElementById("ans");
    ans.value="";
    console.log(ans.value);
}
