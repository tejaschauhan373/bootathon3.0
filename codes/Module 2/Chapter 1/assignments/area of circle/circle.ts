function areaOfCircle()
{
    /*
    *   This function computes the area of circle.
    *   Also check value entered by user is numeric or not.
    *   If it is not numenric it will generate notification to enter numeric  value.
    *   else compute the area of circle and return it.
    */

    let raduis :  HTMLInputElement=<HTMLInputElement>document.getElementById("radius"); 
    let ans : HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("ans"); 


    var r : number=parseFloat(raduis.value);    // convert into float type.
    console.log("radius="+r)


    if(isNaN(r))
    {
        alert("Please enter numeric value only.");  //if user does not enter numeric value, give notification.
    }
    else
    {
        // compute the area of circle
        var area : number=Math.PI*Math.pow(r,2);  //apply MATH.pow function to find square of radius.
        ans.innerHTML="Area of Circle is :"+area.toString(); // convert value of variavble area into string and display it.
    }
}